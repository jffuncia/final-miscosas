from django.contrib import admin

# Register your models here.

from .models import Alimentador, Item, Perfil

admin.site.register(Alimentador)
admin.site.register(Item)
admin.site.register(Perfil)

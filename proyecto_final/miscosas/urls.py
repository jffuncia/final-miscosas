from django.urls import path, include
from . import views
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('', views.index),
    path('logged', views.index),
    path('informacion', views.giveInf),
    path('user/<slug:usuario>', views.userProfile),
    path('user/<str:usuario>/darkmode', views.darkMode),
    path('user/<str:usuario>/letra', views.leterDim),
    path('user_list', views.userList),
    path('alim_list', views.alimList),
    path('alimentador', views.alimentador),
    path('alim_red', views.redirectAlim),
    path('alim/<str:item_id>', views.seeItem),
    path('alim/<str:item_id>/votar', views.votar),
    path('alim/<str:item_id>/comentar', views.addComment),
    path('register', views.register),
    path('login', views.login),
    path('logout', views.logout),
    path('delete', views.delete),
    path('i18n/', include('django.conf.urls.i18n')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

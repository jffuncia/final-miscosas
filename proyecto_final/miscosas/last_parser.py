from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

from .models import Alimentador, Item
class LastHandler(ContentHandler):
    """Class to handle events fired by the SAX parser
    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__ (self):
        self.inEntry = False
        self.inArtist = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.albumLink = ""
        self.artistName = ""
        self.artistLink = ""
        self.id = ""
        self.image= ""
        self.item = []
        self.artist = ""

    def startElement (self, name, attrs):
        if name == 'album':
            self.inEntry = True
        elif self.inEntry:
            if name == 'name':
                self.inContent = True
            elif name == 'url':
                self.inContent = True
            elif name == 'mbid':
                self.inContent = True
            elif name == 'image':
                self.inContent = True
            elif name == 'artist':
                self.inArtist = True
                if name == 'name':
                    self.inContent = True
                elif name == 'url':
                    self.inContent = True

    def endElement (self, name):

        if name == 'album':
            self.inEntry = False
            try:
                artist = Alimentador.objects.get(title=self.artistName)
                artist.canal_items = len(self.item) + 1

            except Alimentador.DoesNotExist:
                artist = Alimentador(title=self.artistName, id = self.id, link=self.artistLink,
                            canal_items=len(self.item), puntuacion=0, type = "Last")
                self.artist = artist
            artist.save()
            a = Alimentador.objects.get(title=self.artistName)

            a = self.image.split('/')
            b = a[-1]
            c = b.split('.')
            idem = c[0]
            try:
                item = Item.objects.get(id = idem)
                self.item.append(item)
            except:
                item = Item(alim=artist,title=self.title, link=self.albumLink, id = idem,
                            description = self.image,likes=0, dislikes=0)

                if self.title != 'null' and self.title != '<Unknown>':
                    item.save()
                    self.item.append(item)

        elif self.inEntry:
            if name == 'artist':
                self.inArtist = False
            elif name == 'name' and not self.inArtist:
                self.title = self.content
            elif name == 'url' and not self.inArtist:
                self.albumLink = self.content
            elif name == 'mbid' and not self.inArtist:
                    self.id = self.content
            elif name == 'name' and self.inArtist:
                self.artistName = self.content
            elif name == 'url' and self.inArtist:
                self.artistLink = self.content
            elif name == 'image'and not self.inArtist:
                self.image = self.content

            self.content = ""
            self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class LastArtist:
    """Class to get videos in a YouTube channel.
    Extracts video links and titles from the XML document for a YT channel.
    The list of videos found can be retrieved lated by calling videos()
    """

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = LastHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)
    def videos (self):

        return self.handler.item

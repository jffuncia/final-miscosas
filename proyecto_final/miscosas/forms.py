from django import forms
from django.contrib.auth.models import User
from .models import Perfil

class takeImage(forms.ModelForm):

    class Meta:
        model = Perfil
        fields = ('foto',)

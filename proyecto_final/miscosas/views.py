from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.contrib.auth import authenticate
from django.contrib.auth import login as do_login
from django.contrib.auth import logout as do_logout
from .models import Alimentador, Item, Item_voted, Comentario, Perfil
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .youtube_parser import YTChannel
from .last_parser import LastArtist
from .wiki_parser import WikiArticle
from .key import LASTFM_APIKEY
from django.contrib.auth.models import User
from .forms import takeImage
import operator
import urllib
import json
from django.conf import settings
from django.utils import translation

def register(request):
    formreg = UserCreationForm()
    form = AuthenticationForm()
    Alimentadores = Alimentador.objects.all()
    top10 = top10items()

    if request.method == "POST":
        formreg = UserCreationForm(data=request.POST)
        if formreg.is_valid():
            usuario = formreg.save()
            perfil = Perfil(usuario= usuario)
            perfil.save()
            if usuario is not None:
                do_login(request, usuario)
                return redirect('/miscosas/logged')

    formularios = {'formreg': formreg, 'form': form}
    content = {'Alimentadores': Alimentadores, 'top10': top10}
    diccionarios = {**content, **formularios}

    return render(request, "miscosas/register.html", diccionarios)

def index(request):
    Alimentadores = Alimentador.objects.all()
    user = request.user
    try:
        last5 = list(Item_voted.objects.filter(usuario=user))[-5:]
    except:
        last5 =""
    try:
        perfil = Perfil.objects.get(usuario = user)
    except:
        perfil = Perfil.objects.none()
    try:
        foto = Perfil.objects.get(usuario = user).foto
    except:
        foto = '/images/defecto.jpg'
    top10 = top10items()
    content = {'Alimentadores': Alimentadores, 'foto': foto, 'top10': top10, 'perfil': perfil, 'last5': last5}

    if request.method == 'GET':
        form = request.GET.get('form')
        if form != '':
            if form == "json":
                alimList =[]
                for alim in Alimentadores:
                    alimList.append(str(alim))
                topList =[]
                for top in top10:
                    topList.append(str(top))
                lastList =[]
                for last in last5:
                    lastList.append(str(last))
                content = {'Alimentadores': alimList, 'top10': topList, 'last5': lastList}
                contents = json.dumps(content)
                return HttpResponse(contents, content_type="application/json")

            elif form == "xml":
                return render(request, 'miscosas/principal.xml', content, content_type='application/xml')

    if request.user.is_authenticated:
        return render(request, "miscosas/index.html", content )

    return redirect('/miscosas/login')

def login(request):
    form = AuthenticationForm()
    Alimentadores = Alimentador.objects.all()
    top10 = top10items()
    try:
        last5 = list(Item_voted.objects.filter(usuario=request.user))[-5:]
    except:
        last5 =""
    try:
        perfil = Perfil.objects.get(usuario = request.user)

    except:
        perfil = Perfil.objects.none()
    try:
        foto = Perfil.objects.get(usuario = request.user).foto
    except:
        foto = '/images/defecto.jpg'
    content = {'Alimentadores': Alimentadores, 'foto': foto,'top10': top10, 'perfil': perfil, 'last5': last5}

    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                try:
                    perfil = Perfil.objects.get(usuario = user)
                    perfil.visits = perfil.visits + 1
                    perfil.save()
                except:
                    perfil = Perfil.objects.none()
                do_login(request, user)
                return redirect('/miscosas/logged')

    diccionarios ={**content, **{'form': form}}
    return render(request, "miscosas/index.html",diccionarios)


def logout(request):
    do_logout(request)
    return redirect('/miscosas')

def top10items ():
    best = {}
    lista_items = []
    items = Item.objects.all()

    for element  in items:
        votos = Item_voted.objects.filter(item = element)
        likes = votos.filter(voto = 1).count()
        dislikes = votos.filter(voto = -1).count()
        total = likes - dislikes
        best[element.link] = total
    orden = sorted(best.items(), key=operator.itemgetter(1), reverse = True)
    orden = orden[0:10]
    for id, votos in orden:
        try:
            item = Item.objects.get(link=id)
        except Item.DoesNotExist:
            return HttpResponse("Insertar error")
        lista_items.append(item)
    return lista_items

def parse(request, id, action):
    if action == 'YouTube':
        if id != '':
            xml = "https://www.youtube.com/feeds/videos.xml?channel_id=" + id
            xmlStream = urllib.request.urlopen(xml)
            channel = YTChannel(xmlStream)
    elif action == 'Last':
        if id != '':
            xml = "http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=" + id + "&api_key=" + LASTFM_APIKEY
            xmlStream = urllib.request.urlopen(xml)
            channel = LastArtist(xmlStream)
    elif action == 'Wikipedia':
        if id != '':
            nombre = id.split()[0]
            xml = "https://en.wikipedia.org/w/index.php?title=" + nombre + "&action=history&feed=rss"
            xmlStream = urllib.request.urlopen(xml)
            channel = WikiArticle(xmlStream)

def alimentador(request):
    form = AuthenticationForm()
    Alimentadores = Alimentador.objects.all()
    top10 = top10items()
    try:
        last5 = list(Item_voted.objects.filter(usuario=request.user))[-5:]
    except:
        last5 =""
    try:
        perfil = Perfil.objects.get(usuario = request.user)
    except:
        perfil = Perfil.objects.none()
    try:
        foto = Perfil.objects.get(usuario=request.user).foto
    except:
        foto = '/images/defecto.jpg'

    if request.method == 'POST':
        action = request.POST['Alimentador']
        id = request.POST['id']
        try:
            if action == 'YouTube':
                alim = Alimentador.objects.get(id=id)
            if action == 'Wikipedia':
                alim = Alimentador.objects.get(title=id)
            elif action =="actualizar":
                alim = Alimentador.objects.get(id=id)
                action = alim.type
                if alim.type == "Last":
                    id = alim.title
                parse(request, id, action)
                if action == 'YouTube':
                    alim = Alimentador.objects.get(id=id)
                else:
                    alim = Alimentador.objects.get(title=id)
            else:
                alim = Alimentador.objects.get(title=id)


        except:
            parse(request, id, action)
            if action == 'YouTube':
                alim = Alimentador.objects.get(id=id)
            else:
                alim = Alimentador.objects.get(title=id)
        alim.selected = True
        alim.save()
    Items = Item.objects.filter(alim=alim)
    Items_ord = sorted(Items, key=lambda x: x.fecha)
    if action == 'YouTube':
        destiny = 'miscosas/alimentador.html'
        content = {'channel': alim, 'Videos': Items_ord,
                    'usuario': request.user, 'Alimentadores': Alimentadores, 'foto': foto, 'top10':top10, 'perfil': perfil, 'last5': last5}
    elif action == 'Wikipedia':
        destiny = 'miscosas/wikipedia.html'
        content = {'article': alim, 'Changes': Items_ord,
                    'usuario': request.user, 'Alimentadores': Alimentadores, 'foto': foto, 'top10':top10, 'perfil': perfil, 'last5': last5}
    else:
        destiny = 'miscosas/last.html'
        content = {'artist': alim, 'Albumes': Items_ord,
                    'usuario': request.user, 'Alimentadores': Alimentadores, 'foto': foto, 'top10':top10, 'perfil': perfil, 'last5': last5}

    diccionarios ={**content, **{'form': form}}
    return render(request, destiny, diccionarios)

def redirectAlim(request):
    form = AuthenticationForm()
    Alimentadores = Alimentador.objects.all()
    top10 = top10items()
    try:
        last5 = list(Item_voted.objects.filter(usuario=request.user))[-5:]
    except:
        last5 =""
    try:
        perfil = Perfil.objects.get(usuario = request.user)
    except:
        perfil = Perfil.objects.none()
    try:
        foto = Perfil.objects.get(usuario= request.user).foto
    except:
        foto = '/images/defecto.jpg'

    if request.method == 'POST':
        id = request.POST['id']
        alim = Alimentador.objects.get(id=id)
    if alim.type == 'YouTube':
        destiny = 'miscosas/alimentador.html'
        content = {'channel': alim, 'Videos': Item.objects.filter(alim=alim),
                    'usuario': request.user, 'Alimentadores': Alimentadores, 'foto': foto, 'top10':top10, 'perfil': perfil, 'last5': last5}
    elif alim.type == 'Wikipedia':
        destiny = 'miscosas/wikipedia.html'
        content = {'article': alim, 'Changes': Item.objects.filter(alim=alim),
                    'usuario': request.user, 'Alimentadores': Alimentadores, 'foto': foto, 'top10':top10, 'perfil': perfil, 'last5': last5}
    else:
        destiny = 'miscosas/last.html'
        content = {'artist': alim, 'Albumes': Item.objects.filter(alim=alim),
                    'usuario': request.user, 'Alimentadores': Alimentadores, 'foto': foto, 'top10':top10, 'perfil': perfil, 'last5': last5}

    diccionarios ={**content, **{'form': form}}
    return render(request, destiny, diccionarios)

def alimList(request):
    Alimentadores = Alimentador.objects.all()
    form = AuthenticationForm()
    top10 = top10items()
    try:
        last5 = list(Item_voted.objects.filter(usuario=request.user))[-5:]
    except:
        last5 =""
    try:
        perfil = Perfil.objects.get(usuario = request.user)
    except:
        perfil = Perfil.objects.none()
    try:
        foto = Perfil.objects.get(usuario= request.user).foto
    except:
        foto = '/images/defecto.jpg'
    content = { 'Alimentadores': Alimentadores, 'foto': foto, 'top10':top10, 'perfil': perfil, 'last5': last5}

    diccionarios ={**content, **{'form': form}}
    return render(request, 'miscosas/alim_list.html', diccionarios)

def seeItem(request, item_id):
    Alimentadores = Alimentador.objects.all()
    top10 = top10items()
    form = AuthenticationForm()
    item = get_object_or_404(Item, id=item_id)
    try:
        last5 = list(Item_voted.objects.filter(usuario=request.user))[-5:]
    except:
        last5 =""
    try:
        perfil = Perfil.objects.get(usuario = request.user)
    except:
        perfil = Perfil.objects.none()
    try:
        foto = Perfil.objects.get(usuario=request.user).foto
        if foto != '/defecto.jpg':
            foto = '/'+str(foto)
        else:
            foto = '/images/defecto.jpg'
    except:
        foto = '/images/defecto.jpg'
    try:
        Voto = Item_voted.objects.filter(usuario = request.user).get(item = item)
    except:
        Voto = Item_voted.objects.none()
    Comentarios = Comentario.objects.filter(item=item)

    if item.alim.type == "YouTube":
        direction = "https://www.youtube.com/embed/" + item_id
        content = {'video': item, 'voto': Voto, 'direction': direction,
                'Alimentadores': Alimentadores, 'Comentarios': Comentarios, 'foto': foto, 'top10': top10, 'perfil': perfil, 'last5': last5}
        redirect = 'miscosas/play_video.html'
    elif item.alim.type == "Wikipedia":
        content = {'change': item, 'voto': Voto, 'Alimentadores': Alimentadores,
                    'Comentarios': Comentarios, 'foto': foto, 'top10': top10, 'perfil': perfil, 'last5': last5}
        redirect = 'miscosas/see_article.html'
    else:
        content = {'album': item, 'voto': Voto,'Alimentadores': Alimentadores,
                'Comentarios': Comentarios, 'foto': foto, 'top10': top10, 'perfil': perfil, 'last5': last5}
        redirect = 'miscosas/see_album.html'

    diccionarios ={**content, **{'form': form}}
    return render(request, redirect, diccionarios)

def delete(request):
    if request.method == 'POST':
        id = request.POST['id']
        channel = Alimentador.objects.get(id=id)
        channel.selected = False
        channel.save()
        if request.user.is_authenticated:
            return redirect('/miscosas/logged')
        else:
            return redirect('/miscosas/')


def votar(request, item_id):
    if request.method == 'POST':
        voto = request.POST['voto']
        item = Item.objects.get(id = item_id)
        usuario = request.user
        try:
            votopas = Item_voted.objects.filter(user = usuario).get(item = item)
            if votopas.voto == -1 and voto == 'like':
                item.dislikes = item.dislikes -1
                item.likes = item.likes + 1
                votopas.voto = 1
                item.save()
                votopas.save()
                item.alim.puntuacion = item.alim.puntuacion + 2
                item.alimentador.save()
            if votopas.voto == 1 and voto == 'dislike':
                item.likes = item.likes -1
                item.dislikes = item.dislikes + 1
                votopas.voto = -1
                item.save()
                votopas.save()
                item.alim.puntuacion = item.alim.puntuacion - 2
                item.alim.save()
            url = '/miscosas/alim/' + item_id
            return redirect(url)
        except:
            if voto == 'dislike':
                item.dislikes = item.dislikes + 1
                votado = Item_voted(usuario = usuario, item = item, voto = -1)
                item.save()
                votado.save()
                item.alim.puntuacion = item.alim.puntuacion - 1
                item.alim.save()
            else:
                item.likes = item.likes + 1
                votado = Item_voted(usuario = usuario, item = item, voto = 1)
                item.save()
                votado.save()
                item.alim.puntuacion = item.alim.puntuacion + 1
                item.alim.save()
            url = '/miscosas/alim/' + item_id
            return redirect(url)

def addComment(request, item_id):
    form = AuthenticationForm()
    if request.method == "POST":
        item = Item.objects.get(id = item_id)
        concepto = request.POST["concepto"]
        comentario = request.POST["comentario"]
        usuario = request.user
        coment = Comentario(usuario = usuario, item = item, concepto = concepto, coment = comentario)
        coment.save()
        url = '/miscosas/alim/' + item_id

    return redirect(url)

def userProfile(request, usuario):
    usuario = User.objects.get(username = usuario)
    try:
        foto = Perfil.objects.get(usuario=usuario).foto
        if foto != '/defecto.jpg':
            foto = '/'+str(foto)
        else:
            foto = '/images/defecto.jpg'
    except:
        foto = '/images/defecto.jpg'
    try:
        last5 = list(Item_voted.objects.filter(usuario=request.user))[-5:]
    except:
        last5 =""
    Alimentadores = Alimentador.objects.all()
    top10 = top10items()
    form = AuthenticationForm()
    usuario = User.objects.get(username = usuario)
    voto = Item_voted.objects.filter(usuario = usuario)
    comentarios = Comentario.objects.filter(usuario = usuario)
    try:
        perfil = Perfil.objects.get(usuario = usuario)
    except:
        perfil = Perfil.objects.none()
    form_img = takeImage()

    if request.method == 'POST':
        form = takeImage(request.FILES)
        if form.is_valid():
            perfil.foto = request.FILES.get('foto')
            perfil.save()
    content = {'usuario': usuario, 'voto': voto, 'comentarios': comentarios,
                'Alimentadores': Alimentadores, 'top10': top10, 'foto': foto, 'perfil': perfil, 'last5': last5}

    diccionarios ={**content, **{'form_img': form_img,'form': form}}
    return render(request, "miscosas/user_profile.html", diccionarios)

def darkMode(request, usuario):
    user = User.objects.get(username = usuario)
    perfil = Perfil.objects.get(usuario = user)
    if request.method == 'POST':
        action = request.POST['Cambiar']
        if action == 'Darkmode':
            perfil.darkmode = True
        elif action == 'Lightmode':
            perfil.darkmode = False
        perfil.save()
    url = '/miscosas/user/' + user.username
    return redirect(url)

def leterDim(request, usuario):
    user = User.objects.get(username = usuario)
    perfil = Perfil.objects.get(usuario = user)
    if request.method == 'POST':
        action = request.POST['Dimension']
        if action == "m":
            perfil.dim_let = "m"
        elif action == "p":
            perfil.dim_let = "p"
        elif action == "g":
            perfil.dim_let = "g"
        perfil.save()
        url = '/miscosas/user/' + user.username
        return redirect(url)

def userList(request):
    Alimentadores = Alimentador.objects.all()
    top10 = top10items()
    form = AuthenticationForm()
    try:
        last5 = list(Item_voted.objects.filter(usuario=request.user))[-5:]
    except:
        last5 =""
    try:
        perfil = Perfil.objects.get(usuario = request.user)
    except:
        perfil = Perfil.objects.none()
    try:
        foto = Perfil.objects.get(usuario= request.user).foto
    except:
        foto = '/images/defecto.jpg'
    usuarios = Perfil.objects.all()
    content = {'usuarios': usuarios, 'Alimentadores': Alimentadores, 'top10': top10, 'foto': foto, 'perfil': perfil, 'last5': last5}

    diccionarios ={**content, **{'form': form}}
    return render(request, 'miscosas/usuarios_list.html', diccionarios)

def giveInf(request):
    Alimentadores = Alimentador.objects.all()
    top10 = top10items()
    form = AuthenticationForm()
    try:
        last5 = list(Item_voted.objects.filter(usuario=request.user))[-5:]
    except:
        last5 =""
    try:
        perfil = Perfil.objects.get(usuario = request.user)
    except:
        perfil = Perfil.objects.none()
    try:
        foto = Perfil.objects.get(usuario= request.user).foto
    except:
        foto = '/images/defecto.jpg'

    content = {'Alimentadores': Alimentadores, 'top10': top10, 'foto': foto, 'perfil': perfil, 'last5': last5}

    diccionarios ={**content, **{'form': form}}
    return render(request, 'miscosas/informacion.html', diccionarios)

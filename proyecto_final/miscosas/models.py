from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.

class Alimentador(models.Model):
    id = models.CharField(max_length=64, primary_key=True)
    title = models.CharField(max_length=64, null=True)
    link = models.CharField(max_length=64, null=True)
    canal_items = models.IntegerField(default=0, null = True)
    puntuacion = models.IntegerField(default=0)
    selected = models.BooleanField(default = True)
    type = models.CharField(max_length=64, null=True)

    def __str__(self):
        return self.title

class Item(models.Model):
    alim = models.ForeignKey(Alimentador, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=64, null=True)
    link = models.CharField(max_length=64, null=True)
    id = models.CharField(max_length=64, primary_key=True)
    description = models.CharField(max_length=256, null=True)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    fecha = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

class Item_voted(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    voto = models.IntegerField(default=0)

    def __str__(self):
        return "El usuario " + self.usuario.username + " ha votado: " + self.item.title

class Comentario(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    concepto = models.CharField(max_length=64, null=True)
    coment = models.TextField(max_length=256, null=True)
    fecha = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "El usuario " + self.usuario.username + " ha comentado: " + self.item.title

class Perfil(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    foto = models.ImageField(upload_to='images/', default = "images/defecto.jpg")
    darkmode = models.BooleanField(default = False)
    dimension = (('p', 'Pequeña'),('m', 'Mediana'),('g', 'Grande'),)
    dim_let = models.CharField(max_length=1, choices=dimension, blank=True, default='m', help_text='Que tamaño quieres para tu letra')
    visits = models.IntegerField(default=0)

    def __str__(self):
        return self.usuario.username

#!/usr/bin/python3

# Simple XML parser for YouTube XML channels
# Jesus M. Gonzalez-Barahona <jgb @ gsyc.es> 2020
# SARO and SAT subjects (Universidad Rey Juan Carlos)
#
# Example of XML document for a YouTube channel:
# https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

from .models import Alimentador, Item
class YTHandler(ContentHandler):
    """Class to handle events fired by the SAX parser
    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.id = ""
        self.link = ""
        self.description= ""
        self.item = []
        self.channel = ""
        self.channelLink = ""
        self.channelName = ""
        self.channelId = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'yt:videoId':
                self.inContent = True
            elif name == 'media:description':
                self.inContent = True
            elif name == 'name':
                self.inContent = True
            elif name == 'uri':
                self.inContent = True
            elif name == 'yt:channelId':
                self.inContent = True


    def endElement (self, name):
        if name == 'entry':
            self.inEntry = False
            try:
                channel = Alimentador.objects.get(title=self.channelName)
                channel.canal_items = len(self.item) + 1
            except Alimentador.DoesNotExist:
                channel = Alimentador(id=self.channelId,title=self.channelName, link=self.channelLink,
                            canal_items=len(self.item), puntuacion=0, type = "YouTube")
                self.channel = channel
            channel.save()

            try:
                item = Item.objects.get(id = self.id)
                self.item.append(item)
            except:
                item = Item(alim=channel,title=self.title, link=self.link, id=self.id,
                        description=self.description,likes=0, dislikes=0)
                item.save()
                self.item.append(item)

        elif self.inEntry:
            if name == 'title':
                self.title = self.content
            elif name == 'yt:videoId':
                self.id = self.content
            elif name == 'media:description':
                self.description = self.content
            elif name == 'name':
                self.channelName = self.content
            elif name == 'uri':
                self.channelLink = self.content
            elif name == 'yt:channelId':
                self.channelId = self.content

            self.content = ""
            self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class YTChannel:
    """Class to get videos in a YouTube channel.
    Extracts video links and titles from the XML document for a YT channel.
    The list of videos found can be retrieved lated by calling videos()
    """

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)
    def videos (self):

        return self.handler.item

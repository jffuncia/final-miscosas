# Generated by Django 3.0.3 on 2020-05-24 22:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('miscosas', '0008_profile_pic'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alimentador',
            name='tipo',
        ),
    ]

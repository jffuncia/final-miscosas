from django.test import TestCase, Client
from django.db.models import QuerySet
from django.template import Template

from . import views
from .models import Alimentador, Perfil, Item
from django.contrib.auth.models import User

# Create your tests here.

class TestYoutube(TestCase):

    def setUp(self):
        self.youtube = 'Alvaro Wasabi'
        self.id_yt = 'wasabihumor'

    def test_youtube(self):
        alimentador = Alimentador(title = self.youtube, id = self.id_yt)
        alimentador.save()
        self.assertEquals(alimentador.id, "wasabihumor")

class TestLast(TestCase):

    def setUp(self):
        self.last = 'Shakira'
        self.id_lt = 'c2b44896d39876813bcbda2b2d45255f'

    def test_last(self):
        alimentador = Alimentador(title = self.last, id = self.id_lt)
        alimentador.save()
        self.assertEquals(alimentador.id, "c2b44896d39876813bcbda2b2d45255f")

class TestWiki(TestCase):

    def setUp(self):
        self.Wiki = 'Fuenlabrada'
        self.id_Wk = 'Fuenlabrada - Revision history'

    def test_Wiki(self):
        alimentador = Alimentador(title = self.Wiki, id = self.id_Wk)
        alimentador.save()
        self.assertEquals(alimentador.id, "Fuenlabrada - Revision history")
        self.assertEquals(alimentador.id, "Test erroneo")

class TestUsu(TestCase):

    def setUp(self):
        self.usuario = User(username = 'juanjuanjo', password = "passworddura")

    def test_usu1(self):
        self.assertEquals(self.usuario.username, "mariano")

    def test_usu2(self):
        self.assertEquals(self.usuario.username, "juanjuanjo")

class TestPerfil(TestCase):

    def setUp(self):
        self.usuario = User(username = 'juanjuanjo')
        self.darkmode = True
        self.letra = 'g'

    def testperfil(self):
        perfil = Perfil(usuario = self.usuario, darkmode = self.darkmode,
                        dim_let = self.letra)

class TestDark(TestCase):

    def setUp(self):
        self.usuario = User(username = 'juanjuanjo')
        self.darkmode = True
        self.letra = 'g'

    def testdark(self):
        perfil = Perfil(usuario = self.usuario, darkmode = False,
                        dim_let = self.letra)
        self.assertEqual(perfil.darkmode, False)
        self.assertEqual(perfil.darkmode, True)

class TestLetra(TestCase):

    def setUp(self):
        self.usuario = User(username='juanjuanjo')
        self.darkmode = True
        self.letra = 'g'

    def testdark(self):
        perfil = Perfil(usuario = self.usuario, darkmode = self.darkmode,
                        dim_let = 'm')
        self.assertEqual(perfil.dim_let, 'p')
        self.assertEqual(perfil.darkmode, 'm')

class TestIndex(TestCase):

    def testIndex(self):
        response = self.client.get('/miscosas/')
        self.assertEqual(response.status_code, 302)

class TestInfo(TestCase):

    def testIndex(self):
        response = self.client.get('/miscosas/informacion')
        self.assertEqual(response.status_code, 200)

class TestItem(TestCase):

    def setUp(self):
        self.alim = Alimentador.objects.create(title = "CursosWeb", type="Youtube" )
        self.item = Item.objects.create(alim = self.alim, title = "Presentación de la práctica final (dudas de alumnos 2)",
                        id = 'HZOodD84MR8')
        self.item.save()

    def testSeeItem(self):
        response = self.client.post('/miscosas/alim/' + self.item.id)
        self.assertEqual(response.status_code, 200)

class TestSeePerfil(TestCase):

    def setUp(self):
        self.usuario = User(username='juanjuanjo')
        self.perfil = Perfil(usuario = self.usuario, darkmode = True, dim_let = 'm')

    def TestSeePerfil(self):
        response = self.client.post('/miscosas/user/' + self.perfil.usuario.username)
        self.assertEqual(response.status_code, 200)

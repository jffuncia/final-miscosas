# final-miscosas

# Práctica Mis Cosas

## Datos

* Nombre: Jesús Fernandez Funcia
* Nombre en el laboratorio: jffuncia
* Titulación: Tecnologías de la Telecomunicación
* Despliegue: http://jffuncia.pythonanywhere.com/miscosas
* Parte obligatoria: https://www.youtube.com/watch?v=3eIa3qC2F7M
* Parte opcional: https://www.youtube.com/watch?v=iL_6Y1UmjcU

## Cuenta Admin Site

* Administrador: admin
* Contraseña: admin

## Cuentas Usuarios

* Usuario 1: juan
* Contraseña: juanjuanito

* Usuario 2: manuel
* Contraseña: manuelmanolo

## Parte obligatoria 
Parte obligatoria con contenidos, usuarios, comentarios, votos, albumes y las demás funcionalidades solicitadas en el enunciado. Se usa el programa Pillow para gestionar las fotos de la práctica.

## Parte Opcional 

* Tercer alimentador sobre articulos de wikipedia y sus modificaciones.
* Implementación del favicon para la página.
* Traducción al inglés con i18n.
* Contador de visitas que te inidca las veces que has entrado a tu usuario.





 
